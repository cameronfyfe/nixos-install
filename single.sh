#!/usr/bin/env bash
set -xuf
trap read debug

USER=cameron

DISK="/dev/$1"
P="parted $DISK"

# !!! May need to edit these based on block device naming (some get ..1,..2,etcsome get ..p1,..p2,etc)
GRUB1="${DISK}p1"
GRUB2="${DISK}p2"
NIX="${DISK}p3"

# Start
sudo $P -- mklabel gpt

# GRUB partitions
sudo $P -- mkpart primary 1MiB 100MiB
sudo $P -- mkpart ESP fat32 101MiB 600MiB

# NixOS partition
sudo $P -- mkpart primary 601MiB 488000MiB

# GRUB partitions flags
sudo $P -- set 1 bios_grub on
sudo $P -- set 2 boot on

# Encrypt NixOS partition
sudo cryptsetup luksFormat $NIX
sudo cryptsetup luksOpen $NIX enc-pv
sudo pvcreate /dev/mapper/enc-pv
sudo vgcreate vg /dev/mapper/enc-pv
sudo lvcreate -n swap vg -L 32G
sudo lvcreate -n root vg -l 100%FREE

# Format GRUB partition
sudo mkfs.vfat -n BOOT $GRUB2

# Format NixOS partition
sudo mkfs.ext4 -L root /dev/vg/root
sudo mkswap -L swap /dev/vg/swap

# Mount NixOS partitions
sudo mount /dev/vg/root /mnt
sudo mkdir /mnt/boot
sudo mount $GRUB2 /mnt/boot
sudo swapon /dev/vg/swap

# Install NixOS 
sudo mkdir -p /mnt/etc/nixos
sudo PATH=$PATH NIX_PATH=$NIX_PATH `which nixos-generate-config` --root /mnt
sudo PATH=$PATH NIX_PATH=$NIX_PATH `which nixos-install` --root /mnt
