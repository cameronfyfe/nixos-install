# nixos-install

This should be able to run from any debian based system, though it's only been tested from an Ubuntu 20.04 live usb.

`single.sh`

Setup:
 - bootloader partitions
 - NixOS partition

`dualboot_and_file_part.sh`

Setup:
 - bootloader partitions
 - NixOS partition
 - partition for 2nd OS
 - shared file partition
