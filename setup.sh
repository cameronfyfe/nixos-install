# Install nix package manager
sudo apt update
sudo apt install curl
curl -L https://nixos.org/nix/install -o ./install.sh
chmod +x ./install.sh
./install.sh --daemon
source $HOME/.nix-profile/etc/profile.d/nix.sh

# Use NixOS Unstable channel
nix-channel --add https://nixos.org/channels/nixos-unstable nixpkgs
nix-channel --update
# Newer Nix versions don't populate NIX_PATH which is needed when running `nixos-install`
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixpkgs"

# Install tools for remote nixos setup
nix-env -i nixos-install-tools

# Pull in nixos config submodule
git submodule update --init
