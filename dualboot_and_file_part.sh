#!/usr/bin/env bash
set -xuf
trap read debug

source ./setup.sh

USER=cameron

DISK="/dev/$1"
P="sudo parted $DISK"

# Start
$P -- mklabel gpt

# GRUB use partitions
GRUB1="${DISK}p1"
$P -- mkpart primary 1MiB 100MiB
GRUB2="${DISK}p2"
$P -- mkpart ESP fat32 101MiB 600MiB

# NixOS partition
NIX="${DISK}p3"
$P -- mkpart primary 601MiB 1000000MiB

# Dualboot OS partition
DUAL="${DISK}p4"
$P -- mkpart primary 1000001MiB 1500000MiB

# File partition
FILE="${DISK}p5"
$P -- mkpart primary fat32 1500001MiB 1900000MiB

# GRUB partitions flags
$P -- set 1 bios_grub on
$P -- set 2 boot on

# Encrypt NixOS partition
sudo cryptsetup luksFormat $NIX
sudo cryptsetup luksOpen $NIX enc-pv
sudo pvcreate /dev/mapper/enc-pv
sudo vgcreate vg /dev/mapper/enc-pv
sudo lvcreate -n swap vg -L 10G
sudo lvcreate -n root vg -l 100%FREE

# Format GRUB partition
sudo mkfs.vfat -n BOOT $GRUB2

# Format Dualboot OS partition
sudo mkfs.ext4 $DUAL

# Format File partition
sudo mkfs.vfat $FILE

# Install NixOS on NixOS partition
sudo mkfs.ext4 -L root /dev/vg/root
sudo mkswap -L swap /dev/vg/swap

sudo mount /dev/vg/root /mnt
sudo mkdir /mnt/boot
sudo mount $GRUB2 /mnt/boot
sudo swapon /dev/vg/swap

sudo mkdir -p /mnt/etc/nixos
sudo cp -a ./nixos/. /mnt/etc/nixos/
sudo mv /mnt/etc/nixos/hardware-configuration.nix /mnt/etc/nixos/hardware-configuration.nix.old

sudo $(which nixos-generate-config) --root /mnt

sudo groupadd -g 30000 nixbld
sudo useradd -u 30000 -g nixbld -G nixbld nixbld

sudo PATH="$PATH" NIX_PATH="${NIX_PATH:-}" $(which nixos-install) --root /mnt
